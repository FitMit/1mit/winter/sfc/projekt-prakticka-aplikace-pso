## Extraction and preprocesing of data from MNIST dataset
# @file process_MNIST.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 20.11.2021

import gzip
import argparse
import numpy as np


parser = argparse.ArgumentParser()

# Parameters
parser.add_argument('train_images_path', type=str, help='Path to train images')
parser.add_argument('train_labels_path', type=str, help='Path to train labels')
parser.add_argument('test_images_path', type=str, help='Path to test images')
parser.add_argument('test_labels_path', type=str, help='Path to test labels')
parser.add_argument('save_prefix', type=str, help='Profix to name of save file')
parser.add_argument('num_train', type=int, help='Number of train vectors')
parser.add_argument('num_test', type=int, help='Number of test vectors')

# Parse
args = parser.parse_args()

############### Read image files ################
# MNIST data sets

####### train-images-idx3-ubyte.gz
f = gzip.open(args.train_images_path,'r')

# Skip magic number
f.read(4)

# Read number of images, number of rows and number of columns
buffer = f.read(4)
num_train_images = int.from_bytes(buffer, 'big')

buffer = f.read(4)
num_train_rows = int.from_bytes(buffer, 'big')

buffer = f.read(4)
num_train_cols = int.from_bytes(buffer, 'big')

print("train-images-idx3-ubyte.gz\nnumber of images="+str(num_train_images)+"\nnumber of rows="+str(num_train_rows)+"\nnumber of columns="+str(num_train_cols))

# Read images
buffer = f.read(num_train_rows * num_train_cols * num_train_images)
train_images = np.frombuffer(buffer, dtype=np.uint8).astype(np.float32)
train_images = train_images.reshape(num_train_images, num_train_rows, num_train_cols, 1)

print("\n")

####### t10k-images-idx3-ubyte.gz
f = gzip.open(args.test_images_path,'r')

# Skip magic number
f.read(4)

# Read number of images, number of rows and number of columns
buffer = f.read(4)
num_test_images = int.from_bytes(buffer, 'big')

buffer = f.read(4)
num_test_rows = int.from_bytes(buffer, 'big')

buffer = f.read(4)
num_test_cols = int.from_bytes(buffer, 'big')

print("t10k-images-idx3-ubyte.gz\nnumber of images="+str(num_test_images)+"\nnumber of rows="+str(num_test_rows)+"\nnumber of columns="+str(num_test_cols))

# Read images
buffer = f.read(num_test_rows * num_test_cols * num_test_images)
test_images = np.frombuffer(buffer, dtype=np.uint8).astype(np.float32)
test_images = test_images.reshape(num_test_images, num_test_rows, num_test_cols, 1)

############ Read label files ##############
print("\n")

####### train-labels-idx1-ubyte.gz
f = gzip.open(args.train_labels_path,'r')

# Skip magic number
f.read(4)

# Read number of labels
buffer = f.read(4)
num_train_labels = int.from_bytes(buffer, 'big')

print("train-labels-idx1-ubyte.gz\nnumber of labels="+str(num_train_labels))

# Read labels
buffer = f.read(num_train_labels)
train_labels = np.frombuffer(buffer, dtype=np.uint8).astype(np.int64)
train_labels = train_labels.reshape(num_train_labels, 1)

print("\n")

####### t10k-labels-idx1-ubyte.gzz
f = gzip.open(args.test_labels_path,'r')

# Skip magic number
f.read(4)

# Read number of labels
buffer = f.read(4)
num_test_labels = int.from_bytes(buffer, 'big')

print("t10k-labels-idx1-ubyte.gz\nnumber of labels="+str(num_test_labels))

# Read labels
buffer = f.read(num_test_labels)
test_labels = np.frombuffer(buffer, dtype=np.uint8).astype(np.int64)
test_labels = test_labels.reshape(num_test_labels, 1)

# Select only specified number of vectors from test/train data

train_images = train_images[:args.num_train]
train_labels = train_labels[:args.num_train]
test_images = test_images[:args.num_test]
test_labels = test_labels[:args.num_test]

num_test_images = args.num_test
num_test_labels = args.num_test
num_train_images = args.num_train
num_train_labels = args.num_train

################## Resize images from 28x28 to 20x20 resolution ####################x
resolution_before = num_train_cols
resolution_after = 20

resized_train_images = np.empty((num_train_images, resolution_after, resolution_after))
resized_test_images = np.empty((num_test_images, resolution_after, resolution_after))


# Resize train images
for x in range(num_train_images):
    print("Resizing "+str(x+1)+". image in train images")

    image = train_images[x]

    # New image
    resized_image = np.empty((resolution_after, resolution_after))
    resized_image[:] = np.NaN

    for i in range(num_train_rows):
        for j in range(num_train_cols):
            # Calculate new pixel position in resized image
            new_position_i = int(i*(resolution_after/resolution_before))
            new_position_j = int(j*(resolution_after/resolution_before))

            # If pixel is empty store new value
            # Because new image is smaller multiple original pixel will by maped to same pixel in new image. So new value is averaged from ariginal pixels
            if np.isnan(resized_image[new_position_i][new_position_j]):
                resized_image[new_position_i][new_position_j] = image[i][j]
            else:
                resized_image[new_position_i][new_position_j] = (image[i][j] + resized_image[new_position_i][new_position_j])/2

    # Store image
    resized_train_images[x] = resized_image

# Resize test images
for x in range(num_test_images):
    print("Resizing "+str(x+1)+". image in test images")

    image = test_images[x]

    # New image
    resized_image = np.empty((resolution_after, resolution_after))
    resized_image[:] = np.NaN

    for i in range(num_test_rows):
        for j in range(num_test_cols):
            new_position_i = int(i*(resolution_after/resolution_before))
            new_position_j = int(j*(resolution_after/resolution_before))

            if np.isnan(resized_image[new_position_i][new_position_j]):
                resized_image[new_position_i][new_position_j] = image[i][j]
            else:
                resized_image[new_position_i][new_position_j] = (image[i][j] + resized_image[new_position_i][new_position_j])/2

    # Store image
    resized_test_images[x] = resized_image

############### Change data structure for clasificator needs ###################

print("\nChanging data format")

formated_train_images = np.empty((num_train_images, resolution_after*resolution_after))
formated_test_images = np.empty((num_test_images, resolution_after*resolution_after))

# Unroll image lines to one 400 items array
for x in range(num_train_images):
    # Concatenate line arrays
    list_of_arrays = list()
    for line in resized_train_images[x]:
        list_of_arrays.append(line)
    formated_train_images[x] = np.concatenate(list_of_arrays)

for x in range(num_test_images):
    # Concatenate line arrays
    list_of_arrays = list()
    for line in resized_test_images[x]:
        list_of_arrays.append(line)
    formated_test_images[x] = np.concatenate(list_of_arrays)

# Normalize values to range 0 - 1
# Now pixel values are 0 to 255. 0 means background (white), 255 means foreground (black).
for x in range(num_train_images):
    for i in range(len(formated_train_images[x])):
        formated_train_images[x][i] = formated_train_images[x][i]/255

for x in range(num_test_images):
    for i in range(len(formated_test_images[x])):
        formated_test_images[x][i] = formated_test_images[x][i]/255

# Convert labels to new format (exmaple [4] -> [0,0,0,0,1,0,0,0,0,0])
# One hot encoding
formated_train_labels = np.empty((num_train_labels, 10))
formated_test_labels = np.empty((num_test_labels, 10))

for x in range(num_train_labels):
    image_class = train_labels[x][0]

    # Number on image can by only 0-9
    index_array = np.zeros((10))

    index_array[image_class] = 1

    formated_train_labels[x] = index_array

for x in range(num_test_labels):
    image_class = test_labels[x][0]

    # Number on image can by only 0-9
    index_array = np.zeros((10))

    index_array[image_class] = 1

    formated_test_labels[x] = index_array

############# Store data ##############x
print("\nSaving resized images and labels")

# Save numpy arrays
np.save(args.save_prefix+"-train_data.npy", formated_train_images)
np.save(args.save_prefix+"-train_labels.npy", formated_train_labels)
np.save(args.save_prefix+"-test_data.npy", formated_test_images)
np.save(args.save_prefix+"-test_labels.npy", formated_test_labels)

# END process_MNIST.py