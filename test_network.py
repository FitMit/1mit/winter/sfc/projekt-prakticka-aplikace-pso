## Test network
# @file test_network.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 23.11.2021

import numpy as np
import argparse
import pickle
import Network

# Instantiate the parser
parser = argparse.ArgumentParser()

# Parameters
parser.add_argument('network', type=str, help='Path to network in format .pzy')
parser.add_argument('test_data', type=str, help='Path to test data in .npy formar')
parser.add_argument('test_labels', type=str, help='Path to labels for test data in .npy formar')

# Parse
args = parser.parse_args()

# Load data
test_data = np.load(args.test_data)
test_labels = np.load(args.test_labels)

# Load network
with open(args.network, "rb") as fp: 
    network = pickle.load(fp)


Network.evaluate_classificator(network, test_data, test_labels)

# END test_network.py