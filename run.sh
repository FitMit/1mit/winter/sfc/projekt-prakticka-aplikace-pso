#!/bin/sh

# Install 
pip install numpy

# Get datasets
wget -P data/ https://archive.ics.uci.edu/ml/machine-learning-databases/00236/seeds_dataset.txt

wget -P data/ http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
wget -P data/ http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
wget -P data/ http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
wget -P data/ http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz

# Run own dataset
echo
echo "############################# Own small dataset #############################"
python3 own_dataset.py
echo
echo "Gradient descend"
python3 run_GD.py 2 2 2 100 0.3 0.1 "own-train_data.npy" "own-train_labels.npy" "own-train_data.npy" "own-train_labels.npy"
echo
echo "Particle swarm optimalization"
python3 run_PSO.py 2 2 2 5 5 1 15 1.4 1.4 0.1 0.1 0.005 10 -10 "own-train_data.npy" "own-train_labels.npy" "own-train_data.npy" "own-train_labels.npy"
echo
# Run seeds dataset
echo "############################# Seeds dataset #############################"
python3 process_seeds.py "data/seeds_dataset.txt" "seeds" 200 10
echo
echo "Gradient descend"
python3 run_GD.py 7 5 3 100 0.3 1 "seeds-train_data.npy" "seeds-train_labels.npy" "seeds-test_data.npy" "seeds-test_labels.npy"
echo
echo "Particle swarm optimalization"
python3 run_PSO.py 7 5 3 5 5 1 15 1.4 1.4 0.7 0.1 0.005 10 -10 "seeds-train_data.npy" "seeds-train_labels.npy" "seeds-test_data.npy" "seeds-test_labels.npy"
echo

# Run MNIST dataset
# Traint data = 100
# Test data = 10
echo "############################# MNIST dataset #############################"
python3 process_MNIST.py "data/train-images-idx3-ubyte.gz" "data/train-labels-idx1-ubyte.gz" "data/t10k-images-idx3-ubyte.gz" "data/t10k-labels-idx1-ubyte.gz" "MNIST" 100 10
echo
echo "Gradient descend"
python3 run_GD.py 400 25 10 100 0.3 5 "MNIST-train_data.npy" "MNIST-train_labels.npy" "MNIST-test_data.npy" "MNIST-test_labels.npy"
echo
#python3 run_PSO.py 400 25 10 25 25 1 15 1.4 1.4 0.7 0.1 0.005 10 -10 "MNIST-train_data.npy" "MNIST-train_labels.npy" "MNIST-test_data.npy" "MNIST-test_labels.npy"