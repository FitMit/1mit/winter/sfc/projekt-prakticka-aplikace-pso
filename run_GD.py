## Learn network by GD (Gradiend Descend)
# @file run_GD.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 20.11.2021

import numpy as np
import random
import pickle
import argparse
from datetime import datetime

import Network

# Initialize random generator
random.seed(datetime.now())

############# Parse args ####################
# Instantiate the parser
parser = argparse.ArgumentParser()

# Parameters
parser.add_argument('input_layer', type=int, help='Number of neurons in input layer')
parser.add_argument('hidden_layer', type=int, help='Number of neurons in hidden layer')
parser.add_argument('output_layer', type=int, help='Number of neurons in output layer (Number of classes)')
parser.add_argument('epochs', type=int, help='Number of GD epochs')
parser.add_argument('learn_rate', type=float, help='GD learn rate')
parser.add_argument('exit_error', type=float, help='GD exit error')
parser.add_argument('train_data', type=str, help='Path to train data in .npy format')
parser.add_argument('train_labels', type=str, help='Path to labels for train data in .npy format')
parser.add_argument('test_data', type=str, help='Path to test data in .npy format')
parser.add_argument('test_labels', type=str, help='Path to labels for test data in .npy format')
parser.add_argument('--save', action='store_true', help='Save network')

# Parse
args = parser.parse_args()

# Network setup parameters
input_layer_size = args.input_layer
hidden_layer_size = args.hidden_layer
output_layer_size = args.output_layer
epochs = args.epochs 
learn_rate = args.learn_rate # Normaly 0.1 to 0.9
exit_error = args.exit_error

############# Load train and test data ###############

train_data = np.load(args.train_data)
train_labels = np.load(args.train_labels)
test_data = np.load(args.test_data)
test_labels = np.load(args.test_labels)

############ Run GD #####################

# Build network and randomly initialize weights
network = Network.initialize_network(input_layer_size, hidden_layer_size, output_layer_size)

# Train network
Network.run_GD(network, train_data, train_labels, learn_rate, epochs, exit_error)

# Test networks succes rate on test data
Network.evaluate_classificator(network, test_data, test_labels)

# Save trained network
if args.save == True:
    print("Saving network")
    with open("GD_network_save__"+datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p"), "wb") as fp:
        pickle.dump(network, fp)

# END run_GD.py
