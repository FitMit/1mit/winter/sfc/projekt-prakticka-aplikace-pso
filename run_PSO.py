## Learn network by PSO (Particle Swarm Optimalization)
# @file run_PSO.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 20.11.2021

from math import inf
import numpy as np
import random
import pickle
import argparse
from datetime import datetime

import PSO
import Network

# Initialize random generator
random.seed(datetime.now())

############# Parse args ####################
# Instantiate the parser
parser = argparse.ArgumentParser()

# Parameters
parser.add_argument('input_layer', type=int, help='Number of neurons in input layer')
parser.add_argument('hidden_layer', type=int, help='Number of neurons in hidden layer')
parser.add_argument('output_layer', type=int, help='Number of neurons in output layer (Number of classes)')
parser.add_argument('epochs', type=int, help='Number of PSO epochs')
parser.add_argument('iterations', type=int, help='Number of swarm creations')
parser.add_argument('exit_error', type=float, help='PSO exit error')
parser.add_argument('swarm_size', type=int, help='PSO swarm size')
parser.add_argument('cognitive_coefficient', type=float, help='Particle cognitive coefficient')
parser.add_argument('social_coefficient', type=float, help='Particle social coefficient')
parser.add_argument('inertia_coeficient', type=float, help='Particle inertia coeficient')
parser.add_argument('velocity_percentage', type=float, help='Particle velocity percentage')
parser.add_argument('teleport_prob', type=float, help='Particle teleport prob')
parser.add_argument('max_weight', type=float, help='Particle maximal weight')
parser.add_argument('min_weight', type=float, help='Particle minimal weight')
parser.add_argument('train_data', type=str, help='Path to train data in .npy format')
parser.add_argument('train_labels', type=str, help='Path to labels for train data in .npy format')
parser.add_argument('test_data', type=str, help='Path to test data in .npy format')
parser.add_argument('test_labels', type=str, help='Path to labels for test data in .npy format')
parser.add_argument('--save', action='store_true', help='Save network')

# Parse
args = parser.parse_args()

# Network setup parameters
input_layer_size = args.input_layer
hidden_layer_size = args.hidden_layer
output_layer_size = args.output_layer
epochs = args.epochs 
iterations = args.iterations
exit_error = args.exit_error
swarm_size = args.swarm_size # Normally 10 - 100
cognitive_coefficient = args.cognitive_coefficient # Normaly cognitive and social same. Values 1.193, 1.494, 2.05
social_coefficient = args.social_coefficient
inertia_coeficient = args.inertia_coeficient # Normaly 0.4 - 0.9
velocity_percentage = args.velocity_percentage # Normaly 10% or 20% - 30% (of Xmax - Xmin)
teleport_prob = args.teleport_prob # Normaly 0.005
max_weight = args.max_weight # 10
min_weight = args.min_weight

############# Load train and test data ###############

train_data = np.load(args.train_data)
train_labels = np.load(args.train_labels)
test_data = np.load(args.test_data)
test_labels = np.load(args.test_labels)

############ Run PSO #####################

# Run number of iterations
best_particle = None
best_particle_error = inf
for i in range(iterations):
    # Initialize swarm
    swarm = PSO.initialize_swarm(input_layer_size, hidden_layer_size, output_layer_size, swarm_size, velocity_percentage, min_weight, max_weight)

    # Run swarm for given time of epochs 
    particle = None
    particle_error = None
    particle, particle_error = PSO.run_pso(swarm, epochs, cognitive_coefficient, social_coefficient, inertia_coeficient, min_weight, max_weight, teleport_prob, exit_error, train_data, train_labels)

    # Check if new particle has better global error
    if(best_particle_error > particle_error):
        best_particle = particle
        best_particle_error = particle_error

    # Stop iterations if error reached
    if(best_particle_error < exit_error):
        print("Error satisfied")
        break

    # Statistics
    print('>iteration=%d/%d, best_error=%.3f' % (i+1, iterations, best_particle_error))

# Print best particle found
print("##############")
print("Best particle error: %.3f" % (best_particle_error))

# Transform particle to network structure used in program
for i in range(len(best_particle)):
    for j in range(len(best_particle[i])):
        best_particle[i][j]['weights'] = best_particle[i][j]['best_weights']

        del best_particle[i][j]['best_weights']
        del best_particle[i][j]['velocities']

# Evaluate particle on test data
Network.evaluate_classificator(best_particle, test_data, test_labels)

# Save trained network
if args.save == True:
    print("Saving network")
    with open("PSO_network_save__"+datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p"), "wb") as fp:
        pickle.dump(best_particle, fp)

# END run_PSO.py
