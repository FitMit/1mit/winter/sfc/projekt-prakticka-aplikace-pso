## Extraction and preprocesing of data from seeds dataset
# @file process_seeds.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 20.11.2021
# Used wheat seeds dataset
# https://archive.ics.uci.edu/ml/datasets/seeds

import numpy as np
import argparse

parser = argparse.ArgumentParser()

# Parameters
parser.add_argument('file_path', type=str, help='Path to seeds dataset in txt format')
parser.add_argument('save_prefix', type=str, help='Profix to name of save file')
parser.add_argument('num_train', type=int, help='Number of train vectors')
parser.add_argument('num_test', type=int, help='Number of test vectors')

# Parse
args = parser.parse_args()

############### Read wheat dataset ################

print("Loading data")

dataset = np.loadtxt(args.file_path, dtype=np.float32)

print("Seeds dataset lenght="+str(len(dataset)))

# Randomly shuffe dataset
np.random.shuffle(dataset)

# Extract features and labels
features = np.zeros((len(dataset), len(dataset[0]) - 1))
labels = np.zeros((len(dataset),1))
for i in range(len(dataset)):
    features[i] = dataset[i][:-1].copy()
    labels[i] = dataset[i][-1]

############### Normalize data and format labels ###################

print("\nChanging data format")

# Normalize data to interval 0 - 1
for i in range(len(features[0])):
    # Get max and min
    max_value = features[0][i]
    min_value = features[0][i]
    for j in range(len(features)):
        if max_value < features[j][i]:
            max_value = features[j][i]

        if min_value > features[j][i]:
            min_value = features[j][i]

    # Normalize
    for j in range(len(features)):
        features[j][i] = (features[j][i] - min_value)/(max_value-min_value)


# Convert labels to One hot encoding (exmaple [2] -> [0,1,0])
# Seeds data set has only 3 clases
labels_one_hot = np.empty((len(labels), 3))

for x in range(len(labels)):
    seed_class = labels[x]

    # Number on image can by only 0-9
    index_array = np.zeros((3))

    # -1 to get index for class
    index_array[int(seed_class)-1] = 1

    labels_one_hot[x] = index_array

############# Create test and train vectors ##############
print("\nGeneration of test and train data")

train_data = features[:args.num_train]
train_labels = labels_one_hot[:args.num_train]
test_data = features[-args.num_train:]
test_labels = labels_one_hot[-args.num_train:]

############# Store data ##############
print("\nSaving seeds test and train data and labels")

# Save numpy arrays
np.save(args.save_prefix+"-train_data.npy", train_data)
np.save(args.save_prefix+"-train_labels.npy", train_labels)
np.save(args.save_prefix+"-test_data.npy", test_data)
np.save(args.save_prefix+"-test_labels.npy", test_labels)

# END process_seeds.py