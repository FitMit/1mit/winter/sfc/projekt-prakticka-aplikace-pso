## Functions for making a neural network and training i by GD (Gradiend Descend
# @file Network.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 20.11.2021

import numpy as np
import random
import math

############# Initialize a network ###############
# +1 in weight arrays is because of additional bias
def initialize_network(n_inputs, n_hidden, n_outputs):
    network = list()

    # Initialize weights in range -0.5 to 0.5
    # Array of weight for each neuron in hidden layer
    hidden_layer = [{'weights':[random.random()-0.5 for i in range(n_inputs + 1)]} for i in range(n_hidden)]
    network.append(hidden_layer)

    # Array of weight for each neuron in output layer
    output_layer = [{'weights':[random.random()-0.5 for i in range(n_hidden + 1)]} for i in range(n_outputs)]
    network.append(output_layer)

    return network
 
############## Forward propagate ##################

# Base function
# potencial = sum(weight_i * input_i) + bias
# bias is last item in weights
def base_function(weights, inputs):
    # Input for bias is alwais 1 (1*bias  = bias)
	potencial = weights[-1] 

    # Calculate linear activation function
	for i in range(len(weights)-1):
		potencial += weights[i] * inputs[i]

	return potencial

# Neuron Activation
# Sigmoid function used
# output = 1 / (1 + e^(-activation))
def activation_function(potencial):
    if potencial > 60:
        return 1
    elif potencial < -60:
        return 0
    else:
        return 1.0 / (1.0 + math.exp(-potencial))

# Forward Propagation
# Go through each layer and calculate layer output 
def forward_propagate(network, input_feature_vector):
	inputs = input_feature_vector

    # Calculate outputs of neurons in layer
	for layer in network:
		output_array = []

        # Calculate output of neuron and add it to array
		for neuron in layer:
			potencial = base_function(neuron['weights'], inputs)

            # Store output to neuron structure
			neuron['output'] = activation_function(potencial)
			output_array.append(neuron['output'])

        # Output of one layer is input for next layer
		inputs = output_array

	return inputs

###################### Back Propagate Error ##########################

# Transfer Derivative
# Calculates transfer value from neuron activation function derivative
# derivative = output * (1.0 - output)
def transfer_derivative(output):
	return output * (1.0 - output)

# Error Backpropagation
# Calculate error for output neuron
# error = (output - expected) * transfer_derivative(output)
#
# Calculate error for neuron in hidden layer
# error = (weight_k * error_j) * transfer_derivative(output)
def backward_propagate_error(network, expected):
    # Calculate error for each neuron. Errors are calculated from last(output) to first layer
	for i in reversed(range(len(network))):
		layer = network[i]

        # List of errors for each neuron
		errors = list()

        # Check which layer is calculated
		if i != len(network)-1:
            # For neurons in hidden layers error is calculated as weighted error from neuron errors in next layer
			for j in range(len(layer)):
				error = 0.0
				for neuron in network[i + 1]:
					error += (neuron['weights'][j] * neuron['delta'])

				errors.append(error)
		else:
            # For neurons in output layer error is calculated as actual output minus expected output
			for j in range(len(layer)):
				neuron = layer[j]
				errors.append(neuron['output'] - expected[j])

        # Multiply error by derivative of neurons activation function and store result as "delta" to neuron
		for j in range(len(layer)):
			neuron = layer[j]
			neuron['delta'] = errors[j] * transfer_derivative(neuron['output'])


###################### Train network ##########################

# Update Weights
# Update weights in network with deltas calculated with backpropagation of error
# weight = weight - learning_rate * error * input

# Update network weights with error
def update_weights(network, input_feature_vector, learn_rate):
	for i in range(len(network)):
        # Input for first layer
		inputs = input_feature_vector

        # Input for not first layer is output from last layer
		if i != 0:
			inputs = [neuron['output'] for neuron in network[i - 1]]

        # Update weights for each neuron in layer
		for neuron in network[i]:
			for j in range(len(inputs)):
				neuron['weights'][j] -= learn_rate * neuron['delta'] * inputs[j]

            # Weigth update for bias
			neuron['weights'][-1] -= learn_rate * neuron['delta']


# Train Network
# Network is trained using GD (Gradient Descent)
def run_GD(network, train_vectors, train_labels, learn_rate, num_epochs, exit_error):
    # Itarate through learning epochs 
    for epoch in range(num_epochs):
        # Error in epoch
        sum_error = 0

        # Iterate through every train vector 
        for i in range(len(train_vectors)):
            feature_vector = train_vectors[i]
            train_output = train_labels[i]

            # Calculate network responce for train vector
            outputs = forward_propagate(network, feature_vector)

            # Sum square errors between actual output and expected output on output neurons
            sum_error += sum([(train_output[j]-outputs[j])**2 for j in range(len(train_output))])

            # Backpropage error
            backward_propagate_error(network, train_output)

            # Update neuron weights
            update_weights(network, feature_vector, learn_rate)

        # Epoch statistics
        print('>epoch=%d/%d, lrate=%.3f, error=%.3f' % (epoch+1, num_epochs, learn_rate, sum_error))

        # Exit when good error
        if sum_error < exit_error:
            print("Error satisfied")
            return


###################### Clasification ##########################
# Classifi input vector
# Return index of class with highest probability
def classifi_vector(network, feature_vector):
    outputs = forward_propagate(network, feature_vector)
    return outputs.index(max(outputs))

# Evaluate classificator
def evaluate_classificator(network, test_vectors, test_labels):
    # Iterate through test set and calculate percentage of good clasified items
    good_clasifications = 0
    for i in range(len(test_vectors)):
        feature_vector = test_vectors[i]
        expected_output = np.where(test_labels[i] == max(test_labels[i]))[0][0]

        result = classifi_vector(network, feature_vector)

        if expected_output == result:
            good_clasifications += 1

    print("Classification accuracy = "+str(good_clasifications/len(test_vectors)))

# END Network.py