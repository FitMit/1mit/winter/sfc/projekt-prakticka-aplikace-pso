Projekt SFC - Praktická aplikace PSO
---------

Vytvořen template pro tvorbu neuronových síťí s učením Backpropagation. Dále byl aplikován algoritmus PSO na trénování neuronovích sítí za účelem porovnání výkonosti s tradičním způsobem učení (Backpropagation).

Body: 20/30
