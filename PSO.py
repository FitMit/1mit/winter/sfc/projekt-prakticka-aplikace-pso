## PSO (Particle Swarm Optimalization) for neural network learning
# @file PSO.py
# @author Matej Kudera <xkuder04@stud.fit.vutbr.cz>
# @date 20.11.2021

from math import inf
import numpy as np
import random
import Network

# initialize random weights for each particle in swarm
def initialize_swarm(n_inputs, n_hidden, n_outputs, n_particles, velocity_percentage, xmin, xmax):
    swarm = list()

    # Initial weights should by in range Xmin to Xmax.
    # Xki = random(Xmin, Xmax)

    # Max velocity should by normaly 10% or 20% - 30% of Xmax-Xmin
    # Vmax = velocity_percentage*(Xmax-Xmin)
    # Initial velocity should by in range -Vmax to Vmax
    # Vki = random(-Vmax. Vmax)

    vmax = velocity_percentage*(xmax-xmin)

    # Initialize each particle
    for i in range(n_particles):
        network = list()
        
        # Array of weight for each neuron in hidden layer
        hidden_layer = [{'weights':[(random.random()*(xmax-xmin)-xmax) for i in range(n_inputs + 1)]} for i in range(n_hidden)]
        # Add velocities to each neuron
        for j in range(len(hidden_layer)):
            hidden_layer[j]['velocities'] = [(random.random()*(vmax-(-vmax))-vmax) for i in range(n_inputs + 1)]

        network.append(hidden_layer)

        # Array of weight for each neuron in output layer
        output_layer = [{'weights':[(random.random()*(xmax-xmin)-xmax) for i in range(n_hidden + 1)]} for i in range(n_outputs)]
        # Add velocities to each neuron
        for j in range(len(output_layer)):
            output_layer[j]['velocities'] = [(random.random()*(vmax-(-vmax))-vmax) for i in range(n_hidden + 1)]
        network.append(output_layer)

        swarm.append(network)

    return swarm

# Calculate error on input vectors
def sum_square_error(particle, input_vectors, input_labels):
    sum_error = 0
    for i in range(len(input_vectors)):
        feature_vector = input_vectors[i]
        train_output = input_labels[i]

        # Calculate network responce for particle
        outputs = Network.forward_propagate(particle, feature_vector)

        # Sum square errors between actual output and expected output
        sum_error += sum([(train_output[j]-outputs[j])**2 for j in range(len(train_output))])

    return sum_error


# Start otimalization of network
def run_pso(swarm, num_epochs, cognitive_coefficient, social_coefficient, inertia_coeficient, xmin, xmax, teleport_prob, exit_error ,train_vectors, train_labels):
    best_particle = None
    best_particle_error = inf
    particles_best_errors = np.zeros((len(swarm)))
    
    ################# Initialize particles parametrs in swarm ##################
    for x in range(len(swarm)):
        particle = swarm[x]

        # Check particle error for train data
        sum_error = sum_square_error(particle, train_vectors, train_labels)

        # On start best weights for each particle are initial weights
        # Sum start penalty
        for i in range(len(particle)):
            for j in range(len(particle[i])):
                particle[i][j]['best_weights'] = particle[i][j]['weights']

        # Save particle error
        particles_best_errors[x] = sum_error

        # Find best particle
        if best_particle_error > particles_best_errors[x]:
            best_particle = particle
            best_particle_error = particles_best_errors[x]
    
        # Save particle
        swarm[x] = particle

    ######################## Run optimimalization #######################
    # Array of particles order
    particle_order = [i for i in range(len(swarm))]

    # Run optimalization for given number of epochs
    for epoch in range(num_epochs):
        # Make random particle order in every epoch
        random.shuffle(particle_order)

        # Calculate new weights for each particle
        for x in range(len(swarm)):
            particle = swarm[particle_order[x]]

            # Calculate new position if particle
            # For every layer in network
            for i in range(len(particle)):
                # For every neuron in layer
                for j in range(len(particle[i])):
                    # For every weight/velocity of neuron
                    for k in range(len(particle[i][j]['weights'])):
                        # Representations of social and cognitive coeficients in function
                        # Random numbers in range 0 - 1
                        cognitive_coefficient_representation = random.random()
                        social_coefficient_representation = random.random()

                        # Calculate new velocity
                        # vk = inertia_coeficient*vk + cognitive_coefficient*cognitive_coefficient_representation*(xkbest - xk) + social_coefficient*social_coefficient_representation*(xbest - xk)
                        particle[i][j]['velocities'][k] = inertia_coeficient*particle[i][j]['velocities'][k] + cognitive_coefficient*cognitive_coefficient_representation*(particle[i][j]['best_weights'][k] - particle[i][j]['weights'][k]) + social_coefficient*social_coefficient_representation*(best_particle[i][j]['best_weights'][k] - particle[i][j]['weights'][k])

                        # Stop optimalization when particles has almost zero velocity
                        if abs(particle[i][j]['velocities'][k]) < 1.0e-30:
                            print("Epoch stoped for low particle velocity")
                            return best_particle, best_particle_error

                        # Calculate new weights
                        # xk = xk + vk
                        particle[i][j]['weights'][k] = particle[i][j]['weights'][k] + particle[i][j]['velocities'][k]

            # Check particle error with new weights
            sum_error = sum_square_error(particle, train_vectors, train_labels)

            # Update weights if new error is better
            if particles_best_errors[particle_order[x]] > sum_error:
                for i in range(len(particle)):
                    for j in range(len(particle[i])):
                        particle[i][j]['best_weights'] = particle[i][j]['weights']

                #Save particle error
                particles_best_errors[particle_order[x]] = sum_error

            # If particle is even better then global best particle, make it new global best particle
            if best_particle_error > particles_best_errors[particle_order[x]]:
                best_particle = particle
                best_particle_error = particles_best_errors[particle_order[x]]

            # Randomly teleport particle with teleport_prob
            teleport = random.random()
            if teleport < teleport_prob:
                # Calculate new position. Velocities are the same
                for i in range(len(particle)):
                    for j in range(len(particle[i])):
                        for k in range(len(particle[i][j]['weights'])):
                            particle[i][j]['weights'][k] = random.random()*(xmax-xmin)-xmax
                            particle[i][j]['best_weights'][k] = particle[i][j]['weights'][k]

                # Calculate new error
                sum_error = sum_square_error(particle, train_vectors, train_labels)

                # Check if new position has better error
                if particles_best_errors[particle_order[x]] > sum_error:
                    for i in range(len(particle)):
                        for j in range(len(particle[i])):
                            particle[i][j]['best_weights'] = particle[i][j]['weights']

                    particles_best_errors[particle_order[x]] = sum_error

                if best_particle_error > particles_best_errors[particle_order[x]]:
                    best_particle = particle
                    best_particle_error = particles_best_errors[particle_order[x]]    

            # Save particle
            swarm[x] = particle

        # Epoch statistics
        print('>epoch=%d/%d, best_error=%.3f' % (epoch+1, num_epochs, best_particle_error))

        # Exit when good error
        if best_particle_error < exit_error:
            return best_particle, best_particle_error

    # Return particle with best weights
    return best_particle, best_particle_error

# END PSO.py